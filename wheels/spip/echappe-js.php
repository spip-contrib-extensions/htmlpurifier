<?php

/**
 * Fonctions utiles pour la wheel echappe-js
 *
 * @SPIP\Textwheel\Wheel\SPIP\Fonctions
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function echappe_anti_xss($match) {
	static $safehtml;
	if (!is_array($match) or !strlen($match[0])) {
		return '';
	}
	$texte = &$match[0];
	if (preg_match("@^(</?(?!script)[a-z]+(\s+class\s*=\s*['\"][a-z _\s-]+['\"])?\s?/?>[\w\s]*)+$@iS", $texte)) {
		return $texte; // input non filtré, $texte doit être safe !
	}
	if (preg_match("@^<svg\b.*</svg>\s*$@iS", $texte)) {

		$svg = &$texte;
		if (!class_exists('enshrined\svgSanitize\Sanitizer')) {
			spl_autoload_register(function ($class) {
				$prefix = 'enshrined\\svgSanitize\\';
				$base_dir = _DIR_PLUGIN_MEDIAS . 'lib/svg-sanitizer/src/';
				$len = strlen($prefix);
				if (strncmp($prefix, $class, $len) !== 0) {
					return;
				}
				$relative_class = substr($class, $len);
				$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
				if (file_exists($file)) {
					require $file;
				}
			});
		}

		// sanitization can need multiples call
		$maxiter = 10;
		do {
			$size = strlen($svg);
			$sanitizer = new enshrined\svgSanitize\Sanitizer();
			$sanitizer->setXMLOptions(0); // garder les balises vide en ecriture raccourcie

			// Pass it to the sanitizer and get it back clean
			$svg = $sanitizer->sanitize($svg);

			// loger les sanitization
			$trace = '';
			foreach ($sanitizer->getXmlIssues() as $issue) {
				$trace .= $issue['message'] . ' L' . $issue['line'] . "\n";
			}
			if ($trace) {
				spip_log($trace, 'svg' . _LOG_DEBUG);
			}
		} while (strlen($svg) !== $size and $maxiter-- > 0);

		return $svg;
	}

	if (!isset($safehtml)) {
		$safehtml = charger_fonction('safehtml', 'inc', true);
	}
	$texte = $safehtml($texte);
	return $texte;
}
